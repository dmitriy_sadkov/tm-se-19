package ru.sadkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.enumeration.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "sessions")
public final class SessionDTO implements Cloneable {
    @Override
    @Nullable
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    @NotNull
    private String id;

    @Nullable
    @Column(name = "timestamp")
    private Long timeStamp;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Nullable
    @Column(name = "signature")
    private String signature;

}
