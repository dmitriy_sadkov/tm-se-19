package ru.sadkov.tm.service.serviceImpl;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.service.IUserService;
import ru.sadkov.tm.util.HashUtil;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public final class UserService extends AbstractService implements IUserService {

    @Autowired
    @NotNull
    private UserRepository userRepository;

    @Override
    @Transactional
    public void userRegister(@Nullable final User user) {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        user.setId(RandomUtil.UUID());
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }


    @Override
    @Transactional
    public void userRegister(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final User user = new User(login, password, Role.USER);
        user.setId(RandomUtil.UUID());
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userRepository.save(user);
    }

    public boolean login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        if (userRepository.existsByLogin(login)) {
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null || user.getPassword() == null) return false;
            return user.getPassword().equals(HashUtil.hashMD5(password));
        }
        return false;
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return ((List<User>) (userRepository.findAll()));

    }

    @Override
    @NotNull
    public UserDTO convert(@Nullable final User user) {
        @NotNull final UserDTO userDTO = new UserDTO();
        if (user == null) return userDTO;
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Override
    @NotNull
    public List<UserDTO> convertList(@Nullable final List<User> users) {
        @NotNull final List<UserDTO> result = new ArrayList<>();
        if (users == null) return result;
        for (@NotNull final User user : users) {
            result.add(convert(user));
        }
        return result;
    }

    @Override
    public void addTestUsers() {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        clear();
        for (@NotNull final User user : users) {
            userRegister(user);
        }
    }

    @Override
    @Nullable
    public User findOneById(@NotNull final String userId) {
        if (userRepository.findById(userId).isPresent()) return userRepository.findById(userId).get();
        return null;
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }
}
