package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.entity.enumeration.Role;


import java.util.List;

public interface ISessionService {

    @Nullable
    SessionDTO open(@Nullable final String login, @Nullable final String password) ;

    void validate(@Nullable final SessionDTO session) throws Exception;

    void validate(@Nullable final SessionDTO session, Role role) throws Exception;

    boolean isValid(@Nullable final SessionDTO session);


    void close(@Nullable final SessionDTO session);

    void closeAll();

    @Nullable
    List<Session> getListSession(@Nullable final SessionDTO session);

    @Nullable
    User getUser(@NotNull final SessionDTO session);

    void load(@NotNull final List<Session> sessions);

    @NotNull
    List<Session> findAll();
}
