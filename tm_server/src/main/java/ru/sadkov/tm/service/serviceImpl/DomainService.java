package ru.sadkov.tm.service.serviceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sadkov.tm.service.IDomainService;
import ru.sadkov.tm.domain.Domain;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.service.IUserService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@Service
public final class DomainService implements IDomainService {

    @Autowired
    @NotNull
    private IUserService userService;

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Autowired
    @NotNull
    private ITaskService taskService;

    @Autowired
    @NotNull
    private ISessionService sessionService;

    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        sessionService.load(domain.getSessions());
    }

    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setUsers(userService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setSessions(sessionService.findAll());
    }

    public void domainSaveJacksonJSON() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try (FileOutputStream fileOutputStream = new FileOutputStream("tm_server/src/main/domen/domain.json")) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @Nullable final String domainString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if (domainString == null || domainString.isEmpty()) throw new WrongDataException("ERROR");
            fileOutputStream.write(domainString.getBytes());
        }
    }

    public void domainSaveJacksonXML() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try (FileOutputStream fileOutputStream = new FileOutputStream("tm_server/src/main/domen/domain.xml")) {
            @NotNull final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
            @Nullable final String domainString = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if (domainString == null || domainString.isEmpty()) throw new WrongDataException("ERROR");
            fileOutputStream.write(domainString.getBytes());
        }
    }

    public void domainSaveXML() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File("tm_server/src/main/domen/domainFile.xml"));
    }

    public void domainSaveJSON() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File("tm_server/src/main/domen/domainFileJSON.json"));
    }

    public void domainSerialize() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try (@NotNull final FileOutputStream domainFos = new FileOutputStream("tm_server/src/main/domen/domain");
             @NotNull final ObjectOutputStream domainOos = new ObjectOutputStream(domainFos)) {
            domainOos.writeObject(domain);
        }
    }

    public void domainDeserialize() throws Exception {
        @NotNull Domain domain = new Domain();
        try (@NotNull final FileInputStream domainFis = new FileInputStream("tm_server/src/main/domen/domain");
             @NotNull final ObjectInputStream domainOis = new ObjectInputStream(domainFis)) {
            domain = (Domain) domainOis.readObject();
            load(domain);
        }

    }

    public void domainLoadJSON() throws Exception {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(new File("tm_server/src/main/domen/domainFileJSON.json"));
        load(domain);
    }

    public void domainLoadXML() throws Exception {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(new File("tm_server/src/main/domen/domainFile.xml"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        load(domain);
    }

    public void domainLoadJacksonJSON() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readerFor(Domain.class).readValue(new File("tm_server/src/main/domen/domain.json"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        load(domain);
    }

    public void domainLoadJacksonXML() throws Exception {
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        final Domain domain = xmlMapper.readerFor(Domain.class).readValue(new File("tm_server/src/main/domen/domain.xml"));
        if (domain == null) throw new WrongDataException("[ERROR]");
        load(domain);
    }
}
