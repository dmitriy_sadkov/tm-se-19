package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.exception.AccessForbiddenException;
import ru.sadkov.tm.repository.SessionRepository;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.IUserService;
import ru.sadkov.tm.util.RandomUtil;
import ru.sadkov.tm.util.SignatureUtil;

import java.util.List;

@Service
@PropertySource("classpath:application.properties")
public final class SessionService implements ISessionService {

    @Autowired
    @NotNull
    private SessionRepository sessionRepository;

    @Autowired
    @NotNull
    private IUserService userService;

    @Value("${session.cycle}")
    private Integer cycle;
    @Value("${session.salt}")
    private String salt;


    @Override
    @Transactional
    @Nullable
    public SessionDTO open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable Session session;
        if (userService.login(login, password)) {
            @NotNull SessionDTO sessionDTO = new SessionDTO();
            @Nullable final User user = userService.findOneByLogin(login);
            if (user == null) return null;
            sessionDTO.setId(RandomUtil.UUID());
            sessionDTO.setRole(user.getRole());
            sessionDTO.setTimeStamp(System.currentTimeMillis());
            sessionDTO.setUserId(user.getId());
            sessionDTO = sign(sessionDTO);
            session = new Session();
            session.setTimeStamp(sessionDTO.getTimeStamp());
            session.setUser(user);
            session.setId(sessionDTO.getId());
            session.setRole(sessionDTO.getRole());
            session.setSignature(sessionDTO.getSignature());
            sessionRepository.save(session);
            return sessionDTO;
        }
        return null;
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new Exception();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimeStamp() == null) throw new AccessForbiddenException();
        if (session.getRole() == null) throw new AccessForbiddenException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTemp = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTemp);
        if (!check) throw new Exception();
        @Nullable Session ses = null;
        if(sessionRepository.findById(session.getId()).isPresent()) {
            ses = sessionRepository.findById(session.getId()).get();
        }
        if (ses == null) throw new AccessForbiddenException();
        if (!sessionRepository.existsById(session.getId())) throw new AccessForbiddenException();
    }

    @Nullable
    private SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null || signature.isEmpty()) return null;
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) throws Exception {
        if (session == null || role == null) throw new Exception();
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessForbiddenException();
        final boolean check = user.getRole().equals(role);
        if (!check) throw new AccessForbiddenException();
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional
    public void close(@Nullable final SessionDTO session) {
        if (session == null) return;
        @Nullable Session ses = null;
        if(sessionRepository.findById(session.getId()).isPresent()) {
            ses = sessionRepository.findById(session.getId()).get();
        }
        if (ses == null) return;
        sessionRepository.delete(ses);
    }

    @Override
    @Transactional
    public void closeAll() {
        sessionRepository.deleteAll();
    }

    @Override
    public List<Session> getListSession(@Nullable final SessionDTO session) {
        return (List<Session>) sessionRepository.findAll();
    }

    @Override
    @Nullable
    public User getUser(@NotNull final SessionDTO session) {
        @Nullable final User user = userService.findOneById(session.getUserId());
        return user;
    }

    @Override
    public void load(@Nullable final List<Session> sessions) {
        closeAll();
        for (@NotNull final Session session : sessions) {
            sessionRepository.save(session);
        }
    }

    @Override
    @NotNull
    public List<Session> findAll() {
       return (List<Session>) sessionRepository.findAll();
    }
}
