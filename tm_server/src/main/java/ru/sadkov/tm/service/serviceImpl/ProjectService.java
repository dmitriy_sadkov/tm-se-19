package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.entity.enumeration.Status;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.IUserService;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public final class ProjectService extends AbstractService implements IProjectService {


    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @Nullable
    public List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(userId);
        projects.sort(comparator);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> convertList(@Nullable final List<Project> projects) {
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        if (projects == null) return result;
        for (@NotNull final Project project : projects) {
            result.add(convert(project));
        }
        return result;
    }

    @NotNull
    @Override
    public ProjectDTO convert(@Nullable final Project project) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        if (project == null) return projectDTO;
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateCreate(project.getDateCreate());
        projectDTO.setDateEnd(project.getDateEnd());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByNameAndUserId(projectName, userId);
        if (project == null) return null;
        return project.getId();
    }

    @Transactional
    public boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        if (projectRepository.existsByUserIdAndName(userId, projectName)) return false;
        @NotNull final Project project = new Project(projectName, description, userService.findOneById(userId), RandomUtil.UUID());
        projectRepository.save(project);
        return true;
    }

    @Transactional
    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.deleteByUserIdAndName(userId, projectName);
    }

    @Nullable
    public List<Project> findAll(@Nullable final User user) {
        if (user == null) return null;
        return projectRepository.findAllByUserId(user.getId());
    }

    @Transactional
    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        projectRepository.deleteByUserId(user.getId());
    }

    @Transactional
    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String description, @Nullable final String userId) {
        if (oldName == null || newName == null || oldName.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final Project project = projectRepository.findOneByNameAndUserId(oldName, userId);
        if (project == null) return;
        projectRepository.update(userId, project.getId(), description, newName);
    }

    @Nullable
    public  Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        @Nullable final String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOneByNameAndUserId(projectName, currentUser.getId());
    }

    @Nullable
    @Override
    public  List<Project> findProjectsByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        return projectRepository.findProjectsByPart(userId, part);
    }

    @Override
    @Nullable
    public List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        return projectRepository.findProjectsByStatus(userId, status);
    }

    @Override
    @Nullable
    @Transactional
    public String startProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date startDate = new Date();
        projectRepository.startProject(Status.PROCESS, userId, projectName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Nullable
    @Transactional
    public String endProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date endDate = new Date();
        projectRepository.endProject(Status.DONE, userId, projectName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return (List<Project>) projectRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }
}
