package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.dto.TaskDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.entity.enumeration.Status;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.util.RandomUtil;

import java.util.*;

@Service
public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    public @Nullable Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findOneByNameAndUserId(taskName, userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> convertList(@Nullable final List<Task> tasks) {
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        if (tasks == null) return result;
        for (@NotNull final Task task : tasks) {
            result.add(convert(task));
        }
        return result;
    }

    @NotNull
    @Override
    public TaskDTO convert(@Nullable final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        if (task == null) return taskDTO;
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateEnd(task.getDateEnd());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    @Override
    @Nullable
    public List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @NotNull final List<Task> tasks = taskRepository.findAllByUserId(userId);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    @Transactional
    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUser == null) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskRepository.existsByNameAndProjectIdAndUserId(taskName, projectId, currentUser.getId())) return false;
        @NotNull final Task task = new Task(RandomUtil.UUID(), taskName, currentUser, projectService.findOneByName(projectName, currentUser));
        persist(task);
        return true;
    }

    @Override
    @Transactional
    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        taskRepository.deleteByNameAndUserId(taskName, currentUser.getId());
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final User user) {
        if (user == null) return null;
        return taskRepository.findAllByUserId(user.getId());
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final User user) {
        taskRepository.deleteAllByUserId(user.getId());
    }

    @Override
    @Transactional
    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        @Nullable final Project project = projectService.findOneByName(projectName, currentUser);
        if (project == null) return;
        @NotNull final Iterator<Task> iterator = taskRepository.findAllByUserId(currentUser.getId()).iterator();
        while (iterator.hasNext()) {
            @NotNull final Task task = iterator.next();
            if (task.getProject().getId().equals(projectId)) {
                taskRepository.deleteByNameAndUserId(task.getName(), currentUser.getId());
            }
        }
    }

    @Override
    @Transactional
    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId) {
        if (oldName == null || oldName.isEmpty()) return;
        if (newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final Task task = findTaskByName(oldName, userId);
        if (task == null) return;
        taskRepository.update(task.getId(), newName, userId);
    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        return taskRepository.getTasksByPart(userId, part);
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        return taskRepository.findAllByUserIdAndStatus(userId, status);
    }

    @Override
    @Nullable
    @Transactional
    public String startTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final Date startDate = new Date();
        taskRepository.startTask(Status.PROCESS, userId, taskName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Nullable
    @Transactional
    public String endTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final Date endDate = new Date();
        taskRepository.endTask(Status.DONE, userId, taskName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return ((List<Task>) (taskRepository.findAll()));
    }

    @Override
    @Transactional
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        for (@NotNull final Task task : tasks) {
            taskRepository.save(task);
        }
    }
}

