package ru.sadkov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.domain.Domain;

public interface IDomainService {
    void load(@Nullable final Domain domain);

    void export(@Nullable final Domain domain);

    void domainSaveJacksonJSON() throws Exception;

    void domainSaveJacksonXML() throws Exception;

    void domainSaveXML() throws Exception;

    void domainSaveJSON() throws Exception;

    void domainSerialize() throws Exception;

    void domainDeserialize() throws Exception;

    void domainLoadJSON() throws Exception;

    void domainLoadXML() throws Exception;

    void domainLoadJacksonJSON() throws Exception;

    void domainLoadJacksonXML() throws Exception;
}
