package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.dto.TaskDTO;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.entity.enumeration.Status;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.service.serviceImpl.ComparatorService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
@Component
public final class TaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private ComparatorService comparatorService;

    @Nullable
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
       sessionService.validate(session);
        return taskService
                .convert(taskService.findTaskByName(taskName, session.getUserId()));
    }

    @Nullable
    @WebMethod
    public List<TaskDTO> getSortedTaskList(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "comparator") @Nullable final String comparatorName) throws Exception {
        sessionService.validate(session);
        Comparator<Task> comparator = (Comparator<Task>) comparatorService.getComparator(comparatorName);
        return taskService
                .convertList(taskService.getSortedTaskList(session.getUserId(), comparator));
    }

    @WebMethod
    public boolean saveTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        return taskService.saveTask(taskName, projectName, sessionService.getUser(session));
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        sessionService.validate(session);
        taskService.removeTask(taskName,sessionService.getUser(session));
    }

    @Nullable
    @WebMethod
    public List<TaskDTO> findAllTasksForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
       sessionService.validate(session);
        return taskService
                .convertList(taskService.findAll(sessionService.getUser(session)));
    }

    @WebMethod
    public void removeAllTasksForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        taskService.removeAll(sessionService.getUser(session));
    }

    @WebMethod
    public void removeTaskForProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
       sessionService.validate(session);
        taskService.removeTaskForProject(projectName, sessionService.getUser(session));
    }

    @WebMethod
    public void updateTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName) throws Exception {
        sessionService.validate(session);
        taskService.update(oldName, newName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<TaskDTO> getTasksByPart(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "part") @Nullable final String part) throws Exception {
        sessionService.validate(session);
        return taskService
                .convertList(taskService.getTasksByPart(session.getUserId(), part));
    }

    @Nullable
    @WebMethod
    public List<TaskDTO> findTasksByStatus(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "status") @Nullable final Status status) throws Exception {
        sessionService.validate(session);
        return taskService
                .convertList(taskService.findTasksByStatus(session.getUserId(), status));
    }


    @Nullable
    @WebMethod
    public String startTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        sessionService.validate(session);
        return taskService.startTask(session.getUserId(), taskName);
    }

    @Nullable
    @WebMethod
    public String endTask(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        sessionService.validate(session);
        return taskService.endTask(session.getUserId(), taskName);
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        return taskService
                .convertList(taskService.findAll());
    }

    @WebMethod
    public void clearTasks(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        taskService.clear();
    }
}
