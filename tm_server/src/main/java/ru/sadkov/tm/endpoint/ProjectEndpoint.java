package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.entity.enumeration.Status;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.serviceImpl.ComparatorService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
@Component
public final class ProjectEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ComparatorService comparatorService;

    @WebMethod
    @Nullable
    public List<ProjectDTO> getSortedProjectList(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "comparator") @Nullable final String comparatorName) throws Exception {
        sessionService.validate(session);
        Comparator<Project> comparator = (Comparator<Project>) comparatorService.getComparator(comparatorName);
        return projectService
                .convertList(projectService.getSortedProjectList(session.getUserId(), comparator));
    }

    @WebMethod
    @Nullable
    public String findProjectIdByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        return projectService.findProjectIdByName(projectName, session.getUserId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        sessionService.validate(session);
        return projectService.persist(projectName, session.getUserId(), description);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        projectService.removeByName(projectName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> findAllProjectsForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        return projectService
                .convertList(projectService.findAll(sessionService.getUser(session)));
    }

    @WebMethod
    public void removeAllProjectsForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        projectService.removeAll(sessionService.getUser(session));
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
       sessionService.validate(session);
        projectService.update(oldName, newName, description, session.getUserId());
    }

    @Nullable
    @WebMethod
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        return projectService
                .convert(projectService.findOneByName(projectName, sessionService.getUser(session)));
    }


    @Nullable
    @WebMethod
    public List<ProjectDTO> findProjectsByPart(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "part") @Nullable final String part) throws Exception {
        sessionService.validate(session);
        return projectService
                .convertList(projectService.findProjectsByPart(session.getUserId(), part));
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> findProjectsByStatus(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "status") @Nullable final Status status) throws Exception {
       sessionService.validate(session);
        return projectService
                .convertList(projectService.findProjectsByStatus(session.getUserId(), status));
    }


    @Nullable
    @WebMethod
    public String startProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        return projectService.startProject(session.getUserId(), projectName);
    }

    @WebMethod
    public @Nullable String endProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        sessionService.validate(session);
        return projectService.endProject(session.getUserId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        return projectService
                .convertList(projectService.findAll());
    }


    @WebMethod
    public void clearProjects(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        projectService.clear();
    }

}
