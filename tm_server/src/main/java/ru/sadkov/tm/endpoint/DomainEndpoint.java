package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.service.IDomainService;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.service.ISessionService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Component
public final class DomainEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @WebMethod
    public boolean domainSaveJacksonJSON(
            @WebParam(name = "session") @Nullable final SessionDTO session) {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainSaveJacksonJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveJacksonXML(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainSaveJacksonXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveXML(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainSaveXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSaveJSON(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainSaveJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainSerialize(@WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainSerialize();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainDeserialize(@WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainDeserialize();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJSON(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainLoadJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadXML(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainLoadXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJacksonJSON(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainLoadJacksonJSON();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean domainLoadJacksonXML(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws java.lang.Exception {
        try {
            sessionService.validate(session, Role.ADMIN);
            domainService.domainLoadJacksonXML();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
