package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.IUserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Component
public final class SessionEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;



    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public boolean isValid(
            @WebParam(name = "session") @Nullable final SessionDTO session) {
        return sessionService.isValid(session);
    }

    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return true;
        } catch (final java.lang.Exception e) {
            return false;
        }
    }

    @WebMethod
    public boolean closeSessionAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        try {
            sessionService.closeAll();
            return true;
        } catch (final java.lang.Exception e) {
            return false;
        }
    }

    @WebMethod
    public List<Session> getListSession(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return sessionService.getListSession(session);
    }

    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return userService.convert(sessionService.getUser(session));
    }
}
