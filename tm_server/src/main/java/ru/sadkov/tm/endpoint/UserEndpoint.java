package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.enumeration.Role;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.service.ISessionService;
import ru.sadkov.tm.service.IUserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebService
@Component
public final class UserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Nullable
    @WebMethod
    public UserDTO getCurrentUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        return userService.convert(sessionService.getUser(session));
    }


    @WebMethod
    public void userRegister(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        userService.userRegister(login, password);
    }

    @WebMethod
    public SessionDTO login(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return sessionService.open(login, password);
    }


    @NotNull
    @WebMethod
    public List<UserDTO> findAllUsers(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        return userService.convertList(userService.findAll());
    }

    @WebMethod
    public void logout(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        sessionService.close(session);
    }

    @WebMethod
    public void updatePassword(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newPassword") @Nullable final String newPassword) throws Exception, NoSuchAlgorithmException {
        sessionService.validate(session);
    }

    @Nullable
    @WebMethod
    public UserDTO findOneUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        return userService.convert(sessionService.getUser(session));
    }

    @WebMethod
    public boolean updateProfile(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newUserName") @Nullable final String newUserName) throws Exception {
        sessionService.validate(session);
        return true;
    }

    @WebMethod
    public boolean isAuth(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        return sessionService.isValid(session);
    }

    @WebMethod
    public void clearUsers(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }
}
