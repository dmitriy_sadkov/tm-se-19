package ru.sadkov.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.enumeration.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "projects")
public final class Project extends AbstractEntity {

    @NotNull
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    private Date dateCreate;

    @Nullable
    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks;

    public Project(@NotNull final String name, @Nullable final String description, @NotNull final User user, @NotNull String id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.user = user;
        this.dateCreate = new Date();
        this.dateBegin = new Date();
        this.dateEnd = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", status=" + status +
                '}';
    }
}
