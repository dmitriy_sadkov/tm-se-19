package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.enumeration.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "sessions")
public final class Session extends AbstractEntity implements Cloneable {

    @Override
    @Nullable
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    @Nullable
    @Column(name = "timestamp")
    private Long timeStamp;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Nullable
    @Column(name = "signature")
    private String signature;
}
