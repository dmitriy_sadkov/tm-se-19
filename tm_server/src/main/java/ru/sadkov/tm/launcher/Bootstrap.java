package ru.sadkov.tm.launcher;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.endpoint.*;
import ru.sadkov.tm.service.IUserService;

import javax.xml.ws.Endpoint;
import java.security.NoSuchAlgorithmException;

@Component
@PropertySource("classpath:application.properties")
public final class Bootstrap {

    @Value("${server.port}")
    private String port;
    @Value("${server.host}")
    private String host;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private IUserService userService;

    public void init() {
        initUsers();
        initEndpoint();
    }

    private void initEndpoint() {
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(sessionEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initUsers() {
        try {
            userService.addTestUsers();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
