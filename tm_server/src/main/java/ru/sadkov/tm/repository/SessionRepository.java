package ru.sadkov.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.entity.Session;

@Repository
public interface SessionRepository extends CrudRepository<Session,String> {
}
