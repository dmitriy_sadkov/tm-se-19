package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.enumeration.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {

    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);

    @Nullable
    Project findOneByNameAndUserId(@NotNull final String projectName, @NotNull final String userId);

    Boolean existsByUserIdAndName(@NotNull final String userId, @NotNull final String projectName);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String projectName);

    void deleteByUserId(@NotNull final String id);

    @Modifying
    @Query("update Project set description = ?3, name = ?4 where user.id = ?1 and id = ?2")
    void update(@NotNull final String userId, @NotNull final String projectId,
                @NotNull final String description, @NotNull final String newName);

    @Nullable
    @Query("select p from Project p where p.user.id = ?1 and p.name like %?2% or p.description like %?2%")
    List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part);

    @Nullable
    @Query("select p from Project p where p.user.id = ?1 and p.status = ?2")
    List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status);

    @Modifying
    @Query("update Project set status = ?1, dateBegin = ?4 where user.id = ?2 and name = ?3")
    void startProject(@NotNull final Status process, @NotNull final String userId,
                      @NotNull final String projectName, @NotNull final Date startDate);

    @Modifying
    @Query("update Project set status = ?1, dateEnd = ?4 where user.id = ?2 and name = ?3")
    void endProject(@NotNull final Status done, @NotNull final String userId,
                    @NotNull final String projectName, @NotNull final Date endDate);
}
