package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull final String login);

    Boolean existsByLogin(@NotNull final String login);
}
