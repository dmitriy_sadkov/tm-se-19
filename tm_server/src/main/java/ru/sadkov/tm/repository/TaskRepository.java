package ru.sadkov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.enumeration.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    @Nullable
    Task findOneByNameAndUserId(@NotNull final String taskName, @NotNull final String userId);

    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId);

    Boolean existsByNameAndProjectIdAndUserId(@NotNull final String taskName,
                                              @NotNull final String projectId,
                                              @NotNull final String id);

    void deleteByNameAndUserId(@NotNull final String taskName, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String id);

    @Modifying
    @Query("update Task set name = ?2 where user.id = ?3 and id = ?1")
    void update(@NotNull final String id, @NotNull final String newName, @NotNull final String userId);

    @Nullable
    @Query("select t from Task t where t.user.id = ?1 and t.name like %?2% or t.description like %?2%")
    List<Task> getTasksByPart(String userId, String part);

    @Nullable
    List<Task> findAllByUserIdAndStatus(@NotNull final String userId,@NotNull final Status status);

    @Modifying
    @Query("update Task set status = ?1, dateBegin = ?4 where user.id = ?2 and name = ?3")
    void startTask(@NotNull final Status process,
                   @NotNull final String userId,
                   @NotNull final String taskName,
                   @NotNull final Date startDate);

    @Modifying
    @Query("update Task set status = ?1, dateEnd = ?4 where user.id = ?2 and name = ?3")
    void endTask(@NotNull final Status done,
                 @NotNull final String userId,
                 @NotNull final String taskName,
                 @NotNull final Date endDate);
}
