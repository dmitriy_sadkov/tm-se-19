package ru.sadkov.tm;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.sadkov.tm.config.DataConfig;
import ru.sadkov.tm.launcher.Bootstrap;


public class Application {

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        ApplicationContext context = new AnnotationConfigApplicationContext(DataConfig.class);
        context.getBean(Bootstrap.class).init();
        }
    }


