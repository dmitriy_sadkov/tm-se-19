package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;

@UtilityClass
public final class ConnectionUtil {

    @NotNull
    private final static String URL = "jdbc:mysql://localhost:3306/app_db";

    @NotNull
    private final static String USERNAME = "root";

    @NotNull
    private final static String PASSWORD = "root";

    @Nullable
    public static Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            return DriverManager.getConnection(URL,USERNAME,PASSWORD);
        }catch (Exception e){
            return null;
        }


    }
}
