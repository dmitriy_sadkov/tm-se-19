package ru.sadkov.tm;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.sadkov.tm.config.ClientConfig;
import ru.sadkov.tm.launcher.Bootstrap;

/**
 * Hello world!
 */
public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfig.class);
        context.getBean(Bootstrap.class).init();
    }
}
