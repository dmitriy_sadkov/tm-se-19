package ru.sadkov.tm.launcher;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.SessionDTO;
import ru.sadkov.tm.endpoint.SessionEndpoint;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private Scanner scanner;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    private SessionDTO currentSession = null;

    @Autowired
    private ApplicationContext applicationContext;

    @Nullable
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.sadkov.tm.command").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

    @Nullable
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(@Nullable final SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws IllegalAccessException, InstantiationException {
        @Nullable final AbstractCommand abstractCommand = applicationContext.getBean(clazz);
        if (abstractCommand == null) return;
        @Nullable final String commandName = abstractCommand.command();
        @Nullable final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        commandMap.put(commandName, abstractCommand);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            try {
                execute(command);
                System.out.println("[DONE]");
                System.out.println("-----------------------------------");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("-----------------------------------");
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;
        if (abstractCommand.safe() || (sessionEndpoint.isValid(currentSession))) {
            abstractCommand.execute();
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    public void init() {
        if (classes == null || classes.isEmpty()) return;
        try {
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
                registry(clazz);
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
