package ru.sadkov.tm.config;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.sadkov.tm.endpoint.*;

import java.util.Scanner;

@Configuration
@ComponentScan(basePackages = "ru.sadkov.tm")
public class ClientConfig {

    @Bean
    @NotNull
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        @NotNull SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        return sessionEndpoint;
    }

    @Bean
    @NotNull
    public DomainEndpoint domainEndpoint() {
        return new DomainEndpointService().getDomainEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }
}
