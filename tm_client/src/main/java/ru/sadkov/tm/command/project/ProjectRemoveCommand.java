package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String command() {
        return "project-removeByName";
    }

    @Override
    public String description() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String projectName = scanner.nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getCurrentSession() == null)
            throw new WrongDataException("[INCORRECT NAME]");
        taskEndpoint.removeTaskForProject(bootstrap.getCurrentSession(),projectName);
        projectEndpoint.removeProjectByName(bootstrap.getCurrentSession(),projectName);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
