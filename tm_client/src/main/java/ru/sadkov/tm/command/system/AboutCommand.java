package ru.sadkov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
@Component

public final class AboutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Program info";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT INFORMATION]");
        System.out.println("project version: " + Manifests.read("Version"));
        System.out.println("developer: " + Manifests.read("Developer"));
        System.out.println("build number: " + Manifests.read("BuildNumber"));
    }

    @Override
    public boolean safe() {
        return true;
    }
}
