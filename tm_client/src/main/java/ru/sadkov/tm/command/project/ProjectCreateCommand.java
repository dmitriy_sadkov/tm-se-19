package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String projectName = scanner.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = scanner.nextLine();
        if (bootstrap.getCurrentSession() == null) {
            System.out.println("[PLEASE LOGIN]");
            return;
        }
        if (projectEndpoint.createProject(bootstrap.getCurrentSession(),projectName, description)) {
            System.out.println("[OK]");
            return;
        }
        throw new WrongDataException();
    }

    @Override
    public boolean safe() {
        return false;
    }
}
