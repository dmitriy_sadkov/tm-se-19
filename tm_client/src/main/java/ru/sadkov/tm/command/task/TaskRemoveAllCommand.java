package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class TaskRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[TASKS CLEAR]");
        taskEndpoint.removeAllTasksForUser(bootstrap.getCurrentSession());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
