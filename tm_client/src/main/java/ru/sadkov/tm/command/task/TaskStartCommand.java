package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class TaskStartCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-start";
    }

    @Override
    public String description() {
        return "Start task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[CHOOSE TASK TO START]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<TaskDTO> tasks = taskEndpoint
                .findTasksByStatus(bootstrap.getCurrentSession(), Status.PLANNED);
        if (tasks == null || tasks.isEmpty()) throw new WrongDataException("[NO PLANNED TASK]");
        ListShowUtil.showList(tasks);
        @Nullable final String taskName = scanner.nextLine();
        if (taskName == null || taskName.isEmpty()) throw new WrongDataException("[WRONG TASK NAME]");
        @Nullable final String startDate = taskEndpoint
                .startTask(bootstrap.getCurrentSession(), taskName);
        if (startDate == null || startDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T START TASK]");
        System.out.println("[TASK: " + taskName.toUpperCase() + " STARTS]");
        System.out.println(("[START DATE: " + startDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
