package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class ProjectSortByStatusCommand extends AbstractCommand {
    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-sort-by-status";
    }

    @Override
    public String description() {
        return "show projects sorted by status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS SORTED BY STATUS]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException();
        @Nullable final List<ProjectDTO> projects = projectEndpoint
                .getSortedProjectList(bootstrap.getCurrentSession(), "projectStatus");
        ListShowUtil.showList(projects);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
