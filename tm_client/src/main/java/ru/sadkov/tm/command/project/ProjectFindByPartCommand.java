package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class ProjectFindByPartCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;


    @Override
    public String command() {
        return "project-find-by-part";
    }

    @Override
    public String description() {
        return "Find project by part of name or description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SEARCH]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException("[NO USER]");
        System.out.println("[ENTER PART OF NAME OR DESCRIPTION]");
        @Nullable final String part = scanner.nextLine();
        if (part == null || part.isEmpty()) throw new WrongDataException("INVALID NAME OR DESCRIPTION");
        @Nullable final List<ProjectDTO> projectList = projectEndpoint
                .findProjectsByPart(bootstrap.getCurrentSession(), part);
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
