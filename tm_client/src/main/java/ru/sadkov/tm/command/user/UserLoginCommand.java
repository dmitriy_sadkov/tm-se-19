package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.Session;
import ru.sadkov.tm.endpoint.SessionDTO;
import ru.sadkov.tm.endpoint.UserEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component
public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Authorize user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = scanner.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = scanner.nextLine();
        @Nullable SessionDTO session = userEndpoint.login(login, password);
        if(session==null)throw new WrongDataException("[SOMETHING WRONG]");
        bootstrap.setCurrentSession(session);
        System.out.println("[YOU ARE SUCCESSFULLY LOG IN]");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
