package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all project";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[PROJECTS CLEAR]");
        taskEndpoint.removeAllTasksForUser(bootstrap.getCurrentSession());
        projectEndpoint.removeAllProjectsForUser(bootstrap.getCurrentSession());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
