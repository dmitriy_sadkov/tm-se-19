package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class ProjectStartCommand extends AbstractCommand {
    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-start";
    }

    @Override
    public String description() {
        return "Start project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[CHOOSE PROJECT TO START]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<ProjectDTO> projects = projectEndpoint
                .findProjectsByStatus(bootstrap.getCurrentSession(), Status.PLANNED);
        if (projects == null || projects.isEmpty()) throw new WrongDataException("[NO PLANNED PROJECT]");
        ListShowUtil.showList(projects);
        @Nullable final String projectName = scanner.nextLine();
        if (projectName == null || projectName.isEmpty()) throw new WrongDataException("[WRONG PROJECT NAME]");
        @Nullable final String startDate = projectEndpoint
                .startProject(bootstrap.getCurrentSession(), projectName);
        if (startDate == null || startDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T START PROJECT]");
        System.out.println("[PROJECT: " + projectName.toUpperCase() + " STARTS]");
        System.out.println(("[START DATE: " + startDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
