package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-removeByName";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String taskName = scanner.nextLine();
        if (taskName == null || taskName.isEmpty() || bootstrap.getCurrentSession() == null)
            throw new WrongDataException();
        taskEndpoint.removeTask(bootstrap.getCurrentSession(),taskName);
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
