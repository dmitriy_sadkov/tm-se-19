package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        @Nullable final String taskName = scanner.nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = scanner.nextLine();
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty())
            throw new WrongDataException("[INCORRECT NAME]");
        boolean success = taskEndpoint
                .saveTask(bootstrap.getCurrentSession(),taskName, projectName);
        if (!success) throw new WrongDataException("[FAILED]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
