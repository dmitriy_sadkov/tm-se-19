package ru.sadkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.DomainEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;
@Component

public final class DomainJacksonLoadXMLCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "load-jackson-XML";
    }

    @Override
    public String description() {
        return "Load domain from XML with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM XML]");
        if(domainEndpoint.domainLoadJacksonXML(bootstrap.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
