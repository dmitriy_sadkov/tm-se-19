package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-show";
    }

    @Override
    public String description() {
        return "Show project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName =scanner.nextLine();
        @Nullable final ProjectDTO project =projectEndpoint
                .findOneProjectByName(bootstrap.getCurrentSession(),projectName);
        if (project == null) throw new WrongDataException("[INCORRECT NAME]");
        System.out.println(project);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
