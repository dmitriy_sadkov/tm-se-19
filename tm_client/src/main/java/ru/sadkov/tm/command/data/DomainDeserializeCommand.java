package ru.sadkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.DomainEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;
@Component

public final class DomainDeserializeCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "load";
    }

    @Override
    public String description() {
        return "Load domen";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM FILE]");
        if(domainEndpoint.domainDeserialize(bootstrap.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
