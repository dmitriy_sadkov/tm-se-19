package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.UserEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;

@Component
public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "logout";
    }

    @Override
    public String description() {
        return "End user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        userEndpoint.logout(bootstrap.getCurrentSession());
        bootstrap.setCurrentSession(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
