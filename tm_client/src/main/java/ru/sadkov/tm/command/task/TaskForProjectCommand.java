package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.*;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
@Component

public final class TaskForProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "tasks-for-project";
    }

    @Override
    public String description() {
        return "Show tasks for chosen project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASKS FOR PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = scanner.nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getCurrentSession() == null)
            throw new WrongDataException();
        @Nullable final String projectId = projectEndpoint
                .findProjectIdByName(bootstrap.getCurrentSession(),projectName);
        if (projectId == null || projectId.isEmpty()) throw new WrongDataException();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        @Nullable final List<TaskDTO> tasksForUser = taskEndpoint
                .findAllTasksForUser(bootstrap.getCurrentSession());
        if (tasksForUser == null || tasksForUser.isEmpty()) throw new WrongDataException();
        for (@NotNull final TaskDTO task : tasksForUser) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result.isEmpty()) throw new WrongDataException();
        ListShowUtil.showList(result);
    }


    @Override
    public boolean safe() {
        return false;
    }
}
