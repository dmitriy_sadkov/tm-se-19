package ru.sadkov.tm.command;



public abstract class AbstractCommand {

    public AbstractCommand() {
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean safe();
}
