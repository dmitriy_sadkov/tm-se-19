package ru.sadkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.launcher.Bootstrap;
@Component

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command :
                bootstrap.getCommandMap().values()) {
            System.out.println(command.command() + ": "
                    + command.description());
        }
    }

    @Override
    public boolean safe() {
        return true;
    }
}
