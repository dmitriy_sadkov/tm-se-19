package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.UserEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component
public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "update-password";
    }

    @Override
    public String description() {
        return "Update password of current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD UPDATE]");
        if (bootstrap.getCurrentSession() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String newPassword = scanner.nextLine();
        userEndpoint.updatePassword(bootstrap.getCurrentSession(), newPassword);
        System.out.println("[PASSWORD CHANGED]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
