package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class ProjectEndCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-end";
    }

    @Override
    public String description() {
        return "End project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[END PROJECT]");
        System.out.println("[CHOOSE PROJECT TO END]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<ProjectDTO> projects = projectEndpoint
                .findProjectsByStatus(bootstrap.getCurrentSession(), Status.PROCESS);
        if (projects == null || projects.isEmpty()) throw new WrongDataException("[NO PROJECTS IN PROCESS]");
        ListShowUtil.showList(projects);
        @Nullable final String projectName = scanner.nextLine();
        if (projectName == null || projectName.isEmpty()) throw new WrongDataException("[WRONG PROJECT NAME]");
        @Nullable final String endDate = projectEndpoint
                .endProject(bootstrap.getCurrentSession(), projectName);
        if (endDate == null || endDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T END PROJECT]");
        System.out.println("[PROJECT: " + projectName.toUpperCase() + " ENDS]");
        System.out.println(("[END DATE: " + endDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
