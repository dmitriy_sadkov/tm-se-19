package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.UserEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class UserUpdateInformationCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Updating User profile";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[UPDATING PROFILE]");
        if (bootstrap.getCurrentSession() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        System.out.println("[ENTER NEW LOGIN]");
        @NotNull final String newUserName = scanner.nextLine();
        if (userEndpoint.updateProfile(bootstrap.getCurrentSession(), newUserName)) {
            System.out.println("[SUCCESS]");
            return;
        }
        throw new WrongDataException("[SOMETHING WRONG! TRY AGAIN]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
