package ru.sadkov.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = scanner.nextLine();
        if (projectName == null || projectName.isEmpty() || bootstrap.getCurrentSession() == null)
            throw new WrongDataException("[INCORRECT NAME]");
        System.out.println("[ENTER NEW NAME]");
        @Nullable final String newName = scanner.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = scanner.nextLine();
        projectEndpoint.updateProject(bootstrap.getCurrentSession(), projectName, newName, description);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
