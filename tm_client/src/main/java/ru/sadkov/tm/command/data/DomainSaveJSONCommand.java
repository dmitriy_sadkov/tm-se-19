package ru.sadkov.tm.command.data;


import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.DomainEndpoint;
import ru.sadkov.tm.launcher.Bootstrap;

@Component

public final class DomainSaveJSONCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "save-JSON";
    }

    @Override
    public String description() {
        return "Save domain in JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        if(domainEndpoint.domainSaveJSON(bootstrap.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
