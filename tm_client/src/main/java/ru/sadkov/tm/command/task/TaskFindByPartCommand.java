package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class TaskFindByPartCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-find-by-part";
    }

    @Override
    public String description() {
        return "Find task by part of name or description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SEARCH]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException("[NO USER]");
        System.out.println("[ENTER PART OF NAME OR DESCRIPTION]");
        @Nullable final String part = scanner.nextLine();
        if (part == null || part.isEmpty()) throw new WrongDataException("INVALID NAME OR DESCRIPTION");
        @Nullable final List<TaskDTO> tasksList = taskEndpoint
                .getTasksByPart(bootstrap.getCurrentSession(), part);
        ListShowUtil.showList(tasksList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
