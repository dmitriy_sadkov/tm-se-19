package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.User;
import ru.sadkov.tm.endpoint.UserDTO;
import ru.sadkov.tm.endpoint.UserEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class UserShowProfileCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "show-user";
    }

    @Override
    public String description() {
        return "Show current User profile";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[CURRENT USER]");
        if (bootstrap.getCurrentSession() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        @Nullable final UserDTO currentUser = userEndpoint.findOneUser(bootstrap.getCurrentSession());
        System.out.println(currentUser);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
