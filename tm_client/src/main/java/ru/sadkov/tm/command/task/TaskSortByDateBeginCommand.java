package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class TaskSortByDateBeginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-sort-by-begin";
    }

    @Override
    public String description() {
        return "show tasks sorted by begin date";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS SORTED BY BEGIN DATE]");
        if (bootstrap.getCurrentSession() == null) throw new WrongDataException();
        @Nullable final List<TaskDTO> tasks = taskEndpoint
                .getSortedTaskList(bootstrap.getCurrentSession(), "taskDateStart");
        ListShowUtil.showList(tasks);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
