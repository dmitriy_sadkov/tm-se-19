package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.ProjectEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;
import java.util.Scanner;
@Component

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        @Nullable final List<ProjectDTO> projectList = projectEndpoint
                .findAllProjectsForUser(bootstrap.getCurrentSession());
        if (projectList == null || projectList.isEmpty()) throw new WrongDataException("NO PROJECTS");
        System.out.println("[PROJECTS:]");
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
