package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.endpoint.TaskEndpoint;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.launcher.Bootstrap;

import java.util.Scanner;
@Component

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private Scanner scanner;

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        @Nullable final String oldName = scanner.nextLine();
        if (oldName == null || oldName.isEmpty() || bootstrap.getCurrentSession() == null)
            throw new WrongDataException();
        @Nullable final TaskDTO task = taskEndpoint.findTaskByName(bootstrap.getCurrentSession(),oldName);
        if (task == null) throw new WrongDataException("[NO SUCH TASK]");
        System.out.println("[ENTER NEW NAME]");
        @Nullable final String newName = scanner.nextLine();
        if (newName == null || newName.isEmpty()) throw new WrongDataException();
        taskEndpoint.updateTask(bootstrap.getCurrentSession(),oldName, newName);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
